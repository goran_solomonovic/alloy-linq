﻿using Alloy.Busiess.Services;
using StructureMap;
using StructureMap.Pipeline;

namespace Alloy.Business.Infrastructure
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            For<ContactsService>().LifecycleIs<TransientLifecycle>();
        }
    }
}