using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;

namespace Alloy.Models.Blocks
{
    [SiteContentType(GUID = "77D9E7FC-D654-41C1-9EBC-982B38CFD288")] 
    [SiteImageUrl] // Use site's default thumbnail
    public class CompetenceBlock : SiteBlockData
    {
        [Display(Order = 30, GroupName = SystemTabNames.Content)]
        [SelectOne(SelectionFactoryType = typeof(YearsSelectionFactory))]
        public virtual int YearsExperience { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            YearsExperience = 0;
        }
    }

    public class YearsSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            return new List<SelectItem>
            {
                new SelectItem { Text = "No experience", Value = 0 },
                new SelectItem { Text = "One year or less", Value = 1 },
                new SelectItem { Text = "Two years", Value = 2 },
                new SelectItem { Text = "Three years", Value = 3 },
                new SelectItem { Text = "Four years", Value = 4 },
                new SelectItem { Text = "Five years or more", Value = 5 }
            };
        }
    }
}
