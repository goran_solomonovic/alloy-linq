using System.ComponentModel.DataAnnotations;
using Alloy.Business.Rendering;
using EPiServer.Web;
using EPiServer.Core;
using System;
using EPiServer.DataAnnotations;
using Alloy.Models.Blocks;
using EPiServer.Shell.ObjectEditing;
using System.Collections.Generic;

namespace Alloy.Models.Pages
{
    /// <summary>
    /// Represents contact details for a contact person
    /// </summary>
    [SiteContentType(
        GUID = "F8D47655-7B50-4319-8646-3369BA9AF05B",
        GroupName = Global.GroupNames.Specialized)]
    [SiteImageUrl(Global.StaticGraphicsFolderPath + "page-type-thumbnail-contact.png")]
    public class ContactPage : SitePageData, IContainerPage
    {
        [Display(Order = 10, GroupName = Global.GroupNames.Contact)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(Order = 20, GroupName = Global.GroupNames.Contact)]
        public virtual string Phone { get; set; }

        [Display(Order = 30, GroupName = Global.GroupNames.Contact)]
        [EmailAddress]
        public virtual string Email { get; set; }

        [Display(Order = 40, GroupName = Global.GroupNames.Contact)]
        public virtual DateTime? Birthday { get; set; }

        [Display(Order = 50, GroupName = Global.GroupNames.Contact)]
        public virtual DateTime? HiredDate { get; set; }

        [Required]
        [Display(Order = 60, GroupName = Global.GroupNames.Contact)]
        [SelectOne(SelectionFactoryType = typeof(DepartmentSelectionFactory))]
        public virtual int Department { get; set; }

        [Display(Order = 10, GroupName = Global.GroupNames.Competence)]
        public virtual CompetenceBlock JavascriptCompetence { get; set; }

        [Display(Order = 20, GroupName = Global.GroupNames.Competence)]
        public virtual CompetenceBlock ReactCompetence { get; set; }

        [Display(Order = 30, GroupName = Global.GroupNames.Competence)]
        public virtual CompetenceBlock CssHtmlCompetence { get; set; }

        [Display(Order = 40, GroupName = Global.GroupNames.Competence)]
        public virtual CompetenceBlock AspNetCompetence { get; set; }

        [Display(Order = 750, GroupName = Global.GroupNames.Competence)]
        public virtual CompetenceBlock SqlCompetence { get; set; }

        public IEnumerable<CompetenceBlock> AllCompetence => new List<CompetenceBlock> 
        {
            JavascriptCompetence,
            ReactCompetence,
            CssHtmlCompetence,
            AspNetCompetence,
            SqlCompetence
        };
    }    

    public class DepartmentSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            return new List<SelectItem>
            {
                new SelectItem { Text = "Design", Value = DepartmentConst.Design },
                new SelectItem { Text = "Innsikt", Value = DepartmentConst.Innsikt },
                new SelectItem { Text = "Prosjektledelse", Value = DepartmentConst.Prosjektledelse },
                new SelectItem { Text = "RIA", Value = DepartmentConst.Ria },
                new SelectItem { Text = "Rådgivning", Value = DepartmentConst.Raadgivning },
                new SelectItem { Text = "Webapps", Value = DepartmentConst.Webapps }
            };
        }
    }

    public class DepartmentConst
    {
        public const int Design = 1;
        public const int Innsikt = 2;
        public const int Prosjektledelse = 3;
        public const int Ria = 4;
        public const int Raadgivning = 5;
        public const int Webapps = 6;
    }
}
