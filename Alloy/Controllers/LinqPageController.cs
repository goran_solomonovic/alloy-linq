using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Alloy.Busiess.Services;
using Alloy.Models.Pages;
using Alloy.Models.ViewModels;
using EPiServer;

namespace Alloy.Controllers
{
    public class LinqPageController : PageControllerBase<LinqPage>
    {
        private readonly ContactsService _contactsService;
        private readonly IContentRepository _contentRepository;

        public LinqPageController(
            ContactsService contactsService,
            IContentRepository contentRepository)
        {
            _contactsService = contactsService;
            _contentRepository = contentRepository;
        }

        public ActionResult Index(LinqPage currentPage)
        {
            var contacts = _contentRepository
                .GetChildren<ContactPage>(currentPage.ContactsReference).ToList();

            var model = new LinqPageViewModel(currentPage)
            {
                LinqInfoList = new List<LinqInfo>
                {
                    LinqInfo1(contacts),
                    LinqInfo2(contacts),
                    LinqInfo3(contacts),
                    LinqInfo4(contacts),
                    LinqInfo5(contacts),
                    LinqInfo6(contacts),
                    LinqInfo7(contacts),
                    LinqInfo8(contacts),
                    LinqInfo9(contacts),
                    LinqInfo10(contacts)
                }
            };
            return View(model);
        }

        public LinqInfo LinqInfo1(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq1Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo2(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq2Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo3(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq3Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo4(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq4Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo5(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq5Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo6(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq6Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo7(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq7Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo8(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq8Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo9(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq9Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }

        public LinqInfo LinqInfo10(List<ContactPage> contacts)
        {
            var linqInfo = new LinqInfo();

            var sw = new Stopwatch();
            sw.Start();
            _contactsService.Linq10Service(linqInfo, contacts);
            sw.Stop();

            linqInfo.ElapsedTicks = sw.ElapsedTicks;
            return linqInfo;
        }
    }
}
