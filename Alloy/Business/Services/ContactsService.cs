﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alloy.Models.Pages;
using Alloy.Models.ViewModels;

namespace Alloy.Busiess.Services
{
    public class ContactsService
    {
        // Sorter etter Departement og så etter Birthday
        public void Linq1Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "1. ";
            linqInfo.ContactList = contacts;
        }

        // Filtrer bort alle som er i Webapps avdelingen og legg dem i linqInfo.ContactList, DepartmentConst.Webapps
        public void Linq2Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "2. ";
        }

        // Finn den største newbien (den med færrest år erfaring til sammen) og legg den i linqInfo.ContactList
        public void Linq3Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "3. ";
        }

        // Lag en kommaseparert liste ove all Asp.Net erfaring i selskapet og legg i linqInfo.Text
        public void Linq4Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "4. ";
        }

        // Sjekk at alle i selskapet kan bli med ut på byen når vi skal til New York på uavhengighetstur 2. september
        // Legg så True eller False i linqInfo.Text
        public void Linq5Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "5. ";
        }

        // List ut alle konsulenter som har et kunnskapssnitt på over 4.0 og legg disse i linqInfo.ContactList
        public void Linq6Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "6. ";
        }

        // Summer alderen til alle konsulentene og legg den i linqInfo.Text
        public void Linq7Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "7. ";
        }

        // Finn den første konsulenten du finner som har både Javascript over 3 og Asp.Net over 3 og legg den i linqInfo.ContactList
        public void Linq8Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "8. ";
        }

        // List ut alle konsultenter bortset fra de fire eldste sortert etter alder (synkende) og legg disse i linqInfo.ContactList
        public void Linq9Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "9. ";
        }

        // Ta to tilfeldige konsulenter og lag en ny liste med dem. List ut alle items så er felles for nye listen og ansattlisten.
        // Legg så felles listen i linqInfo.ContactList
        public void Linq10Service(LinqInfo linqInfo, List<ContactPage> contacts)
        {
            linqInfo.Title = "10. ";
        }
    }
}