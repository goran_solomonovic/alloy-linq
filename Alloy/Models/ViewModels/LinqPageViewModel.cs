using Alloy.Models.Pages;
using System;
using System.Collections.Generic;

namespace Alloy.Models.ViewModels
{
    public class LinqPageViewModel : PageViewModel<LinqPage>
    {
        public LinqPageViewModel(LinqPage currentPage) : base(currentPage)
        {             
        }

        public IEnumerable<LinqInfo> LinqInfoList { get; set; }
    }

    public class LinqInfo
    {
        public string Title { get; set; }

        public long ElapsedTicks { get; set; }

        public IEnumerable<ContactPage> ContactList { get; set; }

        public string Text { get; set; }
    }
}
